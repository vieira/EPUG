# Python script to translate markdown to different languages using openAI GPT models

## Setup

```sh
python3 -m venv .venv
source .venv/bin/activate
pip install openai click mypy
```

You will need to replace the contents of the `TOKEN` file with a token of your own generated at:
https://platform.openai.com/account/api-keys

## Running

Testing the code with a small input file:

```sh
python translate.py --file smalltext.md --to spanish
python translate.py --file smalltext.md --to french
python translate.py --file smalltext.md --to german
```

Trying out a larger piece of text

```sh
python translate.py --file fulltext.md --to spanish
python translate.py --file fulltext.md --to german
python translate.py --file fulltext.md --to french
python translate.py --file fulltext.md --to klingon
python translate.py --file fulltext.md --to esperanto
python translate.py --file fulltext.md --to japanese
```

After discussion there was a proposal to generalize the prompt to do more than translation.
Some of the prompts tested produced hilarious results.

```sh
python generic_prompt.py --file fulltext.md --to html --prompt "Render the following markdown to HTML and return raw HTML"
python generic_prompt.py --file fulltext.md --to summary --prompt "Summarize the next content into one paragraph"
python generic_prompt.py --file fulltext.md --to rude --prompt "Rewrite the next content into very rude english"
```
