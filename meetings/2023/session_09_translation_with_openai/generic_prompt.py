import os

import click
import openai

# Text in fulltext.md is from https://docs.python.org/3/tutorial/index.html

with open("TOKEN") as fh:
    openai.api_key = fh.read().strip()


@click.command()
@click.option("--file", required=True, help="File to translate")
@click.option("--to", required=True, help="Language to translate to")
@click.option("--model", default="gpt-3.5-turbo", help="GPT language model to use")
@click.option("--prompt", required=True, help="Prompt to condition the model")
def prompt(file, to, model, prompt):
    """Ask any GPT model for any kind of text transformation."""

    with open(file) as fh:
        text = fh.read()

    request = [
        {
            "role": "system",
            "content": prompt,
            # "content": (
            #     f"Translate the following text to {to} "
            #     "returning the result in markdown"
            # ),
        },
        {
            "role": "user",
            "content": text,
        },
    ]

    try:
        output = openai.ChatCompletion.create(
            model=model,
            messages=request,
        )
    except openai.error.RateLimitError:
        print("Try again later, we hit OpenAI's rate limite")
        raise

    print(output["usage"])

    filename, ext = os.path.splitext(file)

    outfile = f"{filename}-{to}{ext}"

    with open(outfile, "w") as out:
        for reply in output["choices"]:
            out.write(reply["message"]["content"])

        print("Translation to", to, "saved to", outfile)


if __name__ == "__main__":
    prompt()
