#!/usr/bin/env python

import asyncio
import time

async def asyncio_gather():
    res = await asyncio.gather(
        asyncio.sleep(1),
        asyncio.sleep(1),
        asyncio.sleep(1),
    )
    print(res)

async def asyncio_loop():
    res = [
        await asyncio.sleep(1),
        await asyncio.sleep(1),
        await asyncio.sleep(1),
    ]
    print(res)

async def main():
    print("Running `asyncio_gather()`...")
    start = time.perf_counter()
    await asyncio_gather()
    end = time.perf_counter()
    print("`asyncio_gather()` took", end - start, "seconds")

    print("Running `asyncio_loop()`...")
    start = time.perf_counter()
    await asyncio_loop()
    end = time.perf_counter()
    print("`asyncio_loop()` took", end - start, "seconds")

asyncio.run(main())
