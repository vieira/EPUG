#!/usr/bin/env python

import asyncio
import time

def time_sleep():
    print("time.sleeping...")
    time.sleep(1)
    return 1

def time_main():
    res = [
        time_sleep(),
        time_sleep(),
        time_sleep(),
    ]
    print(res)

async def asyncio_sleep():
    print("asyncio.sleeping...")
    await asyncio.sleep(1)
    return 1

async def asyncio_main():
    res = await asyncio.gather(
        asyncio_sleep(),
        asyncio_sleep(),
        asyncio_sleep(),
    )
    print(res)

if __name__ == "__main__":
    print("Running `time_main()`...")
    start = time.perf_counter()
    time_main()
    end = time.perf_counter()
    print("`time_main()` took", end - start, "seconds")

    print("Running `asyncio_main()`...")
    start = time.perf_counter()
    asyncio.run(asyncio_main())
    end = time.perf_counter()
    print("`asyncio_main()` took", end - start, "seconds")