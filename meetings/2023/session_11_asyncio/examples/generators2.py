#!/usr/bin/env python

def test():
    val = yield 1
    print(val)
    yield 2
    yield 3

gen = test()

print(next(gen))
print(gen.send("meow"))
# print(next(gen))
# print(next(gen))

