#!/usr/bin/env python

# async for does not run all the items in the generator concurrently!
# Rather, it runs the whole loop concurrently with some other coroutine.

import asyncio

async def my_generator(u: int = 5):
    """Yield powers of 2."""
    i = 0
    while i < u:
        yield 2 ** i
        i += 1
        await asyncio.sleep(1)

async def main():
    async for i in my_generator():
        print(i)

# Runs synchronously
asyncio.run(main())

async def main2():
    await asyncio.gather(main(), main())

# Run concurrently
asyncio.run(main2())
