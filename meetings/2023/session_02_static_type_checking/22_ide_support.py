"""
█ █▀▄ █▀▀  █▀ █░█ █▀█ █▀█ █▀█ █▀█ ▀█▀
█ █▄▀ ██▄  ▄█ █▄█ █▀▀ █▀▀ █▄█ █▀▄ ░█░

- NOW YOUR EDITOR ACTUALLY HAS A CLUE ON WHAT YOU'RE DOING
- GET GREAT AUTO-COMPLETE SUGGESTIONS
"""

class X:
    def foo(self) -> int:
        return 123

    def bar(self, x: int) -> str:
        return "lala"

my_int = X().