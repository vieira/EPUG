def draw_a_rectangle(color: str):
    assert color in ["blue", "red", "grey"] # grey with an 'e'
    ...

# draw_a_rectangle("gray") # typo, ruining your day


# "THE ASSERTION WILL CATCH YOUR ERROR, BUT BY THEN IT'S TOO LATE:"
# "    - YOUR SLURM JOB WAS WASTED;"
# "    - YOUR APP JUST CRASHED IN THE HANDS OF YOUR FAVORITE BIOLOGIST;"
# "    - YOU WASTED YOUR TIME RUNNING TESTS;"
# "    - YOU LEFT YOUR JOB AND/OR DIED AND NOW NO ONE CAN FIX THIS;"


# from typing import Literal
# def draw_a_rectangle2(color: Literal["blue", "red", "grey"]):
#     ...
# draw_a_rectangle2("gray") # static checking got your back



# "AND IT CAN BE FAR LESS OBVIOUS:"

# import sys
# color: str = sys.stdin.read() # THERE IS NO TEST FOR USER BEHAVIOR
# draw_a_rectangle(color) # BUG! WHO KNOWS WHAT COLOR THIS IS

# "BUT!"

# import sys
# color: str = sys.stdin.read()
# draw_a_rectangle2(color) # aha! we don't take just any strings here
# if color == "blue" or color == "grey" or color == "red":
#     draw_a_rectangle2(color) # now this is fine
# else:
#     print("Please enter a valid color!", file=sys.stderr)
