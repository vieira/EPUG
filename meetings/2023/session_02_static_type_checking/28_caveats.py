"""
█▀▀ ▄▀█ █░█ █▀▀ ▄▀█ ▀█▀ █▀
█▄▄ █▀█ ▀▄▀ ██▄ █▀█ ░█░ ▄█
"""

# '- It can be frustrating in the beginning'
# '    - But you can do it gradually =D'

# '- It is not "simple"'
# '    - Let me know when you stumble on "generics" and "variance"'

# "- You have to keep two worlds in mind:"
# "    - the one before your code runs"
# "        - (i.e.: what the type checker can know before your code runs)"
# '    - the one as your code is running (i.e.: what the code actually does)'

# '    ... and they can differ in surprising ways.'

# "- There's kind of an entire second language on top of python"
# "  that is composed of type hints"

# "- python's type system has many, many holes"

# "- numeric code is harder to make type-safe without a"
# "  language that supports it properly"

# "    - but you *can* say that .e.g. your function only takes"
# "      numpy arrays with 3 dimensions and type uint8"