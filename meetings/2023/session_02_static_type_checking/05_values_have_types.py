"""
█░█ ▄▀█ █░░ █░█ █▀▀ █▀   █░█ ▄▀█ █░█ █▀▀   ▀█▀ █▄█ █▀█ █▀▀ █▀
▀▄▀ █▀█ █▄▄ █▄█ ██▄ ▄█   █▀█ █▀█ ▀▄▀ ██▄   ░█░ ░█░ █▀▀ ██▄ ▄█
"""
from typing import List

some_int = 123
some_float = 3.14
some_string = "hello"
a_list_of_strings = ["a", "b", "c"]

class Penguin:
    def swim(self):
        ...
a_penguin = Penguin()


""" So, implicitly: """


some_int: int = 123
some_float: float = 3.14
some_string: str = "hello"
a_list_of_strings: List[str] = ["a", "b", "c"]
a_penguin: Penguin = Penguin()
