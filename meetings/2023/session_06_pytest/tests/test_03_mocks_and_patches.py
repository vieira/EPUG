"""
    Pytest: mocks and patches.
"""
from email.message import EmailMessage
import smtplib
from unittest.mock import call, ANY, Mock, patch

import pytest

def send_email(client: smtplib.SMTP, message: EmailMessage) -> None:
    """
    Send e-mail to the user.

    WARNING: this function will send the actual email, be careful!
    """
    client.send_message(message)

def notify_admin() -> None:
    """
    Send dangerous e-mail to the admin.

    Note that the client is initialized in the function code and not passed
    as an argument. Also the host name is hardcoded.
    """
    client = smtplib.SMTP(host='smtp.embl.de', port=25)
    # NB: this is not a valid message
    client.send_message(EmailMessage())

# ---

def test_send_email():
    """
    Test the e-mail sender.
    """
    fake_client = Mock()

    msg = EmailMessage()
    msg['Subject'] = 'CHEAP C1ALiS'
    msg['From'] = 'noreply@foobar.com'
    msg['To'] = 'support@embl.de'
    msg.set_content('Now in angebot!')
    # NB: what can be improved here?

    send_email(fake_client, msg)
    assert len(fake_client.mock_calls) == 1

    expected_call = call.send_message(msg)
    assert fake_client.mock_calls[0] == expected_call

def test_notify_admin():
    """
    Test the dangerous admin notifier.
    """
    with patch('smtplib.SMTP') as fake_smtp:
        # NB: this function must be refactored
        notify_admin()

    assert fake_smtp.mock_calls[0] == call(host='smtp.embl.de', port=25)
    assert fake_smtp.mock_calls[1] == call().send_message(ANY)
    assert isinstance(fake_smtp.mock_calls[1].args[0], EmailMessage)
