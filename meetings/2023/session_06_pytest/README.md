Pytest basics
=============
EPUG session by Danil Kister (danil.kister@embl.de).

Quickstart
----------
* Clone the repo:
  ```
  $ git clone git@git.embl.de:grp-bio-it/EPUG.git
  cd EPUG/meetings/2023/session_03_pytest
  ```
* Create a virtual environment:
  ```
  $ python -m venv .venv
  # Linux/MacOS
  $ source .venv/bin/activate
  # Windows
  $ .venv\Scripts\activate
  ```
* Install dependencies:
  ```
  $ pip install -r requirements.txt
  ```
* Run the included tests:
  ```
  $ pytest -v
  ```
