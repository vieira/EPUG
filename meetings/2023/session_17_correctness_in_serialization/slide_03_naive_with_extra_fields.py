# pyright: strict
import json
from dataclasses import dataclass

@dataclass
class Person:
    name: str
    age: int # ADDED NEW FIELD!!

    def to_json(self) -> str:
        raw_data = {"name": self.name} # OOPS! where is age?
        return json.dumps(raw_data)

    @classmethod
    def from_json(cls, data: str) -> "Person":
        parsed = json.loads(data)
        return Person(name=parsed["name"]) # AHA! pyright saved us here

        # And we have the first insight: deserialization is safer than serialization.
        # "from_json" is flagged as wrong, but "to_json" isn't

# this will break even after fixing "from_json"
# because we forgot to serialize "age"!!!!!!
bob = Person("Bob", 50)
serialized_bob = bob.to_json()
deserialized_bob = Person.from_json(serialized_bob)

