# pyright: strict
import json
from dataclasses import dataclass

@dataclass
class Color:
    hex_code: str

    def to_json(self) -> str:
        return json.dumps(self.__dict__)

    @classmethod
    def from_json(cls, data: str) -> "Color":
        parsed = json.loads(data)
        return Color(parsed["hex_code"])

@dataclass
class Person:
    name: str
    fav_color: Color # Adds A non-primitive field!!

    def to_json(self) -> str:
        # No errors, but WRONG. Where is Color.to_json()?
        return json.dumps(self.__dict__)

    @classmethod
    def from_json(cls, data: str) -> "Person":
        parsed = json.loads(data)
        # Hopefully this looks weird to everyone
        keys = Person("dummy", Color("#ffffff")).__dict__.keys()
        return Person(**{
            # This is super WRONG. Where is Color.from_json?
            key: parsed[key] for key in  keys
        })

ser_person = Person("Bob", fav_color=Color("#ffffff")).to_json()

# So __dict__ is a bad idea.
# It's clunky to use in from_json and it can't handle non-primitive types
# Let's try to fix 'from_json' first