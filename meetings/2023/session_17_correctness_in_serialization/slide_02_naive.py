# pyright: strict
import json
from dataclasses import dataclass

@dataclass
class Person:
    name: str

    def to_json(self) -> str:
        raw_data = {"name": self.name}
        return json.dumps(raw_data)

    @classmethod
    def from_json(cls, data: str) -> "Person":
        parsed = json.loads(data)
        return Person(name=parsed["name"])

#and this works
bob = Person("Bob")
serialized_bob = bob.to_json()
deserialized_bob = Person.from_json(serialized_bob)
assert bob.name == "Bob"
