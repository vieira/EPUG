"CAVEATS"

"""
    - We're focusing on correctness;
        - Which can come at the expense of convenience;

    - We want our code to be automatically checked;

    - We may end up with pepetitive code
      - which is is ok, because it always gets flagged if
        a mistake is made;

    - Dealing with old versions of serialized data is
      its own can of worms;
        - You have to detect old versions and try to convert it;
        - We probably won't cover it today.
"""