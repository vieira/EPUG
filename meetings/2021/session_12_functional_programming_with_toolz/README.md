# Functional programming with python core data structures

### Depenencies
To run the notebooks in this repo, please make sure that you have the following packages installed:

* [toolz](https://pypi.org/project/toolz/0.1/)
* [pandas](https://pypi.org/project/pandas/) 
* [seaborn](https://pypi.org/project/seaborn/)


### Additional resources

* [A curated list of functional programming related projects in python](https://github.com/sfermigier/awesome-functional-python)
* [Matthew Rocklins' (toolz author) talk on toolz](https://vimeo.com/80096814)
* [Currying in Python - Dustin Haffner](https://www.youtube.com/watch?v=L3G7BH7n0K0)
