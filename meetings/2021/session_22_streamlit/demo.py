from io import StringIO

import numpy as np
import pandas as pd
import streamlit as st

st.write("Hello world!")

st.markdown("# Header")

st.markdown("## Button")
b = st.button("Say hello")

if b:
    st.write("Hello button!")

st.markdown("## Numpy arrays")
npdata = np.random.randn(10, 10)
st.table(npdata)

st.markdown("## Slider")
x = st.slider("Pick a value", min_value=2)

st.write(x, "is the value you picked")

st.markdown("## Dataframes")
dataframe = pd.DataFrame(
    np.random.randn(10, x), columns=("col %d" % i for i in range(x))
)

highlight = dataframe.style.highlight_max(axis=0)
st.dataframe(highlight)

st.markdown("## Plotting")
st.line_chart(dataframe)
st.markdown(
    "**Also available**: *Matplotlib*, *Altair*, *Plotly*, *Bokeh*, *PyDeck*, ..."
)

st.markdown("## Input text")
st.markdown("### And using session data")
st.text_input("Enter your string", key="name")

# You can access the value at any point with:
st.markdown(f"**{st.session_state.name}** is your string")
# print(st.session_state.name)

# Callbacks
def form_callback():
    st.write("### callback values")
    st.write(st.session_state.my_slider)
    st.write(st.session_state.my_checkbox)
    st.write("### end of callback values")


with st.form(key="my_form"):
    slider_input = st.slider("My slider", 0, 10, 5, key="my_slider")
    checkbox_input = st.checkbox("Yes or No", key="my_checkbox")
    submit_button = st.form_submit_button(label="Submit", on_click=form_callback)


st.markdown("## File uploads")

uploaded_file = st.file_uploader("Your data")

if uploaded_file is not None:
    stringio = StringIO(uploaded_file.getvalue().decode("utf-8"))
    st.code(stringio.read())

st.markdown("## END")
