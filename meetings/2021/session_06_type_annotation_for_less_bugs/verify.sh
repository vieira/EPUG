#!/usr/bin/env sh

set -e

if [ ! -f helpers/venv/bin/activate ]; then
  ./helpers/make_venv.sh
fi

source helpers/venv/bin/activate

mypy --python-version 3.9 sample.py
