#!/usr/bin/env python
from multiprocessing.dummy import Pool 
#from multiprocessing import Pool 
import socket
import subprocess
import random
from time import sleep

def ping(ip):
    syscall = subprocess.Popen(["ping", "-c", "1", "-W", "1", ip], shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    syscall.wait()
#    sleep(random.randint(1,5))
    print 'Pinging', ip
    return ip #syscall.returncode

if __name__ == '__main__':
    subnet = '.'.join(socket.gethostbyname(socket.gethostname()).split('.')[:-1]) + '.'

    MAX_HOSTS = 254
    pool = Pool(100)
    net = [subnet + str(i) for i in range(1,MAX_HOSTS)]
    result = pool.map(ping, net)
    pool.close()
    print result
