class MyExceptionValue(ValueError):
    def __init__(self, error_code):
        self.error_code = error_code

    def __str__(self):
        return f"The error number was {self.error_code}"

    def __repr__(self):
        return f"<Exception>: The error number was {self.error_code}"


def func():
    raise MyExceptionValue(404)
