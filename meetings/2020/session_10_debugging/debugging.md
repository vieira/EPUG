# Debugging in Python

## Some concepts

* *Breakpoint* - location in a file where the debugger should pause
* *Continue* - resume execution until next breakpoint or unexpected condition
* *Step over* - execute the current line and pause at the beginning of the next
* *Step into* - if the current line is a function call, enter the function and execute from the first line of the function
* *Step out* - execute the current function till the end and pause as soon as the function returns
* *Backtrace* - the execution path up to the location where the debugger is paused

## `pdb` cheat sheet

* `args` - show arguments from surrounding function
* `b` & `break` - set or list breakpoints
* `display` - show tracked variables
* `n` & `next` - execute instruction in next line (step over)
* `s` & `step` - execute next possible instruction (step into)
* `r` & `return` - execute until end of current function (step out)
* `p` & `pp` - print result of expression
* `ll` & `longlist` - show the source code/context of current paused state
* `bt` & `w` & `where` - show the execution path up to the current location

## launching pdb

Add `import pdb ; pdb.set_trace()` to your code and you will get access to `pdb` on the spot.

Alternatively run `python -mpdb script.py` from the command-line.

## VSCode / VSCodium

Install [Python extension](https://marketplace.visualstudio.com/items?itemName=ms-python.python) by Microsoft to have Python goodies and debugging powers.

## Remote debugging & development

The [remote development pack](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) is pretty awesome. You might find the [Remote - SSH](https://aka.ms/vscode-remote/download/ssh) option very useful, together with the [Remote - Containers](https://aka.ms/vscode-remote/download/containers) if you work with a lot with docker and similar solutions.

### Reaching the location where the code is running

This requires some familiarity with network concepts and `SSH`.

You will need to setup port-forwarding (aka tunneling) using `SSH`.  
You might also need to `ProxyJump` a few computers to get to the location where the code is running.

A simple, "one hop" `SSH` tunnel can be achieved with `ssh -L localhost:5678:localhost:5678 login.cluster.embl.de`.

For more about SSH tunnels try [these docs](https://www.ssh.com/ssh/tunneling/example) and [how to use `ProxyJump`](https://en.wikibooks.org/wiki/OpenSSH/Cookbook/Proxies_and_Jump_Hosts#Passing_Through_One_or_More_Gateways_Using_ProxyJump).

### debugpy

`debugpy` is [pretty awesome](https://github.com/microsoft/debugpy) but needs to connect via the network. You'll need to use the `SSH` tunnel setup above.